-- ripped this off hearts view, could you tell =P

local bar_builder = {}

function bar_builder:new(game, config)

  local bar = {}

  if config ~= nil then
    bar.dst_x, bar.dst_y = config.x, config.y
  end

  bar.surface = sol.surface.create(112, 8)
  bar.nb_max_bar_displayed = 0
  bar.nb_current_bar_displayed = 0
  -- bar.img = sol.surface.create("hud/bar_bar.png")
  bar.img = sol.sprite.create(config.img)
  bar.type = config.type
  bar.danger_val = config.danger_val
  bar.danger_percent = config.danger_percent
  bar.danger_snd = config.danger_snd
  bar.transparent = false

  bar.val = 0
  bar.maxVal = 0
  bar.isDanger = false

  function bar:on_started()

    -- This function is called when the HUD starts or
    -- was disabled and gets enabled again.
    -- Unlike other HUD elements, the timers were canceled because they
    -- are attached to the menu and not to the game
    -- (this is because the bar are also used in the savegame menu).

    -- After game-over don't show gradually getting the bar back.
    -- bar.nb_current_bar_displayed = val
    bar:check()
    bar:rebuild_surface()
  end

  -- Checks whether the view displays the correct info
  -- and updates it if necessary.
  function bar:check()

    local need_rebuild = false

	local val, maxVal = 0
	if bar.type == "life" then
		val = game:get_life()
		maxVal = game:get_max_life()

		if (bar.danger_val ~= nil and val <= bar.danger_val) or (bar.danger_percent ~= nil and val <= maxVal * bar.danger_percent / 100) then
			if not bar.isDanger then
				if bar.danger_snd ~= nil then
					sol.audio.play_sound(bar.danger_snd)
				end
				bar.isDanger = true
			end
		else
			bar.isDanger = false
		end
	end
	if bar.type == "magic" then
		val = game:get_magic()
		maxVal = game:get_max_magic()
	end
	
    -- maxValimum bar.
    local nb_max_bar = maxVal
    if nb_max_bar ~= bar.nb_max_bar_displayed then
      need_rebuild = true

      if nb_max_bar < bar.nb_max_bar_displayed then
        -- Decrease immediately if the maxVal bar is reduced.
        bar.nb_current_bar_displayed = val
      end

      bar.nb_max_bar_displayed = nb_max_bar
    end

    -- Current bar.
    local nb_current_bar = val
    if nb_current_bar ~= bar.nb_current_bar_displayed then

      need_rebuild = true
      if nb_current_bar < bar.nb_current_bar_displayed then
        bar.nb_current_bar_displayed = bar.nb_current_bar_displayed - 1
      else
        bar.nb_current_bar_displayed = bar.nb_current_bar_displayed + 1
      end
    end

	bar.val = val
	bar.maxVal = maxVal

    -- Redraw the surface only if something has changed.
    if need_rebuild then
      bar:rebuild_surface()
    end

    -- Schedule the next check.
    sol.timer.start(bar, 10, function()
      bar:check()
    end)
  end

  function bar:rebuild_surface()
    bar.surface:clear()

    -- beginning of bar.
    -- bar.img:draw_region(0, 0, 8, 8, bar.surface, 0, 0)
	bar.img:set_animation("start")
    bar.img:draw(bar.surface)
	local w, h = bar.img:get_size()
	local x = w
	local y = 0

    -- Display the bar.
    for i = 0, (bar.nb_max_bar_displayed - 1) / w do
      -- local x, y = (i % 10) * 8, math.floor(i / 10) * 8

	  -- local offset = 0
	  -- if i == bar.nb_max_bar_displayed - 1 then
		-- offset = 8
	  -- end

      -- if i < math.floor(bar.nb_current_bar_displayed / 8) then
        -- bar.img:draw_region(16, offset, 8, 8, bar.surface, x, 0)
      -- else
		-- bar.img:draw_region(8, offset, 8, 8, bar.surface, x, 0)
        -- bar.img:draw_region(16, offset, bar.nb_current_bar_displayed - i*8, 8, bar.surface, x, 0)
      -- end
	  local offset = w
	  bar.img:set_animation("empty")
	  if (i + 1) <= bar.nb_max_bar_displayed / w then
		bar.img:draw(bar.surface, x, y)
	  else
		offset = bar.nb_max_bar_displayed - (i*w)
		bar.img:draw_region(0, 0, offset, h, bar.surface, x, y)
	  end
	  w, h = bar.img:get_size()
	  if i < bar.nb_current_bar_displayed / w then
		  bar.img:set_animation("fill")
		  if bar.val == bar.maxVal then
			if bar.img:has_animation("max") then
				bar.img:set_animation("max")
			end
		  elseif bar.isDanger then
			if bar.img:has_animation("danger") then
			  bar.img:set_animation("danger")
			end
		  end
		  if (i + 1) <= bar.nb_current_bar_displayed / w then
			bar.img:draw(bar.surface, x, y)
		  else
			bar.img:draw_region(0, 0, bar.nb_current_bar_displayed - (i*w), h, bar.surface, x, y)
		  end
	  end
      x = x + offset
    end

    -- end of bar.
    -- bar.img:draw_region(24, 0, 8, 8, bar.surface, bar.nb_max_bar_displayed * 8 + 8, 0)
	bar.img:set_animation("end")
    bar.img:draw(bar.surface, x, y)
  end

  function bar:set_dst_position(x, y)
    bar.dst_x = x
    bar.dst_y = y
  end

  function bar:get_surface()
    return bar.surface
  end

  function bar:on_draw(dst_surface)

    local x, y = bar.dst_x, bar.dst_y
    local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    -- Everything was already drawn on self.surface.
    bar.surface:set_opacity(bar.transparent and 128 or 255)
    bar.surface:draw(dst_surface, x, y)
  end

  -- Sets if the element is semi-transparent or not.
  function bar:set_transparent(transparent)
    if transparent ~= bar.transparent then
      bar.transparent = transparent
    end
  end

  bar:rebuild_surface()

  return bar
end

return bar_builder
